<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules;

use League\Container\ServiceProvider;
use Laasti\Providers\RoutableProviderInterface;

/**
 * Description of WhoopsProvider
 *
 * @author Sonia
 */
class CrudModulesProvider extends ServiceProvider implements RoutableProviderInterface
{

    protected $provides = [
        'Pixms\CrudModules\CrudService',
        'Pixms\CrudModules\ModulesLoader',
    ];
    
    protected $defaultConfig = [
        'modules' => [
            'Pixms\Modules\Users\UsersModule',
            'Pixms\Modules\Slides\SlidesModule',
        ],
    ];

    public function register()
    {
        $di= $this->getContainer();

        $config = $this->defaultConfig;
        if (isset($di['CrudModules.config']) && is_array($di['CrudModules.config'])) {
            $config = array_merge($config, $di['CrudModules.config']);
        }
        
        if (!$di->isRegistered('Pixms\CrudModules\CrudService')) {
            $di->add('Pixms\CrudModules\CrudService', 'Pixms\CrudModules\CrudService', true);
        }
        
        $di->add('Pixms\CrudModules\ModulesLoader', null, true)->withArguments([$di, $config['modules']]);
    }
    
    public function getRoutes() {
        return [
            ['GET', '/{module:word}', 'Pixms\CrudModules\Controllers\ListController::display'],
            ['GET', '/{module:word}/create', 'Pixms\CrudModules\Controllers\CreateController::display'],
            ['POST', '/{module:word}/create', 'Pixms\CrudModules\Controllers\CreateController::submit'],
            ['GET', '/{module:word}/details/{id:number}', 'Pixms\CrudModules\Controllers\UpdateController::display'],
            ['POST', '/{module:word}/details/{id:number}', 'Pixms\CrudModules\Controllers\UpdateController::submit'],
            ['GET', '/{module:word}/delete/{id:number}', 'Pixms\CrudModules\Controllers\DeleteController::handle'],
            ['GET', '/{module:word}/activate/{id:number}', 'Pixms\CrudModules\Controllers\StatusController::activate'],
            ['GET', '/{module:word}/deactivate/{id:number}', 'Pixms\CrudModules\Controllers\StatusController::deactivate'],
            ['POST', '/{module:word}/reorder', 'Pixms\CrudModules\Controllers\ReorderController::handle'],
        ];
    }

}
