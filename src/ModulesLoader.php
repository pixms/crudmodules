<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules;

use League\Container\ContainerInterface;
use Pixms\CrudModules\Interfaces\ModuleInterface;

/**
 * Description of ModulesLoader
 *
 * @author Sonia
 */
class ModulesLoader
{
    protected $container;
    
    protected $modules = [];
    
    public function __construct(ContainerInterface $container, $modules = [])
    {
        $this->container = $container;
        foreach ($modules as $module) {
            $this->register($module);
        }
    }
    
    public function register($module) {
        if (is_string($module)) {
            $module = $this->container->get($module);
        }
        
        if ($module instanceof ModuleInterface) {
            if (isset($this->modules[$module->getIdentifier()])) {
                throw new \RuntimeException('Name conflict between 2 modules: '.$module->getIdentifier());
            }
            $module->register($this->container);
            $this->modules[$module->getIdentifier()] = $module;
            return $this;
        }
        
        throw new InvalidArgumentException('Your module must implement the ModuleInterface.');
    }
    
    public function getModule($module) {
        return $this->modules[$module];
    }
    
}
