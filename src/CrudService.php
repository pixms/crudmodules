<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules;

use Pixms\DataTables\DataTableFactoryInterface;
use Pixms\Spot\Exceptions\EntityNotFoundException;
use Pixms\Spot\Exceptions\EntityNotSavedException;
use Spot\MapperInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of CrudService
 *
 * @author Sonia
 */
class CrudService
{
    protected $repository;
    protected $decorator;

    public function __construct($repository, \Pixms\CrudModules\Decorators\AbstractDecorator $decorator = null)
    {
        $this->repository = $repository;
        $this->decorator = $decorator;
    }

    public function createFromRequest(Request $request) {
        if ($request->getMethod() !== 'POST') {
            return [
                'data' => [],
                'errors' => []
            ];            
        }
        
        $data = array_merge($request->attributes->all(), $request->request->all(), $request->files->all());
        $entity = $this->repository->build($data);
        $request->attributes->set('entity', $entity);

        if ($this->repository->validate($entity)) {
            
            if (method_exists($entity, 'getOrderableField')) {
                $this->addOrder($entity);
            }
            
            //TODO Move image upload to here, not the repository
            if (!$this->repository->insert($entity, ['validate' => false, 'strict' => false])) {
                throw new EntityNotSavedException;
            }
            
            return true;
        } else {
            //TODO make it an interface
            return [
                'data' => $this->repository->deepData($entity),
                'errors' => $entity->errors()
            ];
        }
    }
    
    public function updateFromRequest(Request $request) {
        $entity = $this->repository->get($request->attributes->get('id'));
        
        if ($request->getMethod() !== 'POST') {
            return [
                'data' => $this->repository->deepData($entity),
                'errors' => []
            ];            
        }
        
        $this->repository->fill($entity, $request);

        if ($this->repository->validate($entity)) {
            
            //TODO Move image upload to here, not the repository
            if ($this->repository->update($entity, ['validate' => false, 'strict' => false]) === false) {
                throw new EntityNotSavedException;
            }
            
            return true;
        } else {
            
            return [
                'data' => $this->repository->deepData($entity),
                'errors' => $entity->errors()
            ];
        }
    }
    
    public function buildDataTable(DataTableFactoryInterface $factory, Request $request) {
        //TODO The data should be retrieved here and not in the factory
        return $factory->create($this->getDataTableData($request));
        
    }
    
    public function getDataTableData($request) {
        //TODO decorate the results here with a class wrapper with a cursor
        $results = $this->repository->all();
        
        //Reorderable entities need to be sorted by priority
        if (method_exists($this->repository->entity(), 'getOrderableField')) {
            $order_field = call_user_func(array($this->repository->entity(), 'getOrderableField'));  
            $results->order([$order_field => 'ASC']);
        }
        return new Decorators\CollectionDecorator($results, $this->decorator);
    }
    
    //TODO Move to its own status service
    public function activateEntityById($id) {
        
        $entity = $this->repository->get($id);

        if ($entity === false) {
            throw new EntityNotFoundException();
        }

        if ($entity->status !== 1) {
            $entity->status = 1;

            if (!$this->repository->save($entity)) {
                throw new EntityNotSavedException();
            }
            
            return true;
        } else {
            return false;
        }
        
    }
    
    //TODO Move to its own status service
    public function deactivateEntityById($id) {
        
        $entity = $this->repository->get($id);

        if ($entity === false) {
            throw new EntityNotFoundException();
        }

        if ($entity->status !== 0) {
            $entity->status = 0;

            if (!$this->repository->save($entity)) {
                throw new EntityNotSavedException();
            }
            
            return true;
        } else {
            return false;
        }
        
    }
    
    public function deleteEntityById($id) {
        if (!$this->repository->delete(array('id' => $id))) {
            throw new EntityNotFoundException();
        }
        
        return true;
    }
    
    public function reorderEntities($direction, $from, $to) {
        $order_field = call_user_func(array($this->repository->entity(), 'getOrderableField'));
        
        $entity = $this->repository->where([$order_field => $from])->first();

        if ($entity === false) {
            throw new EntityNotFoundException();
        }

        $entity->priority = $to;
        
        $table = $this->repository->table();
        
        if ($direction == 'forward') {
            $result = (bool) $this->repository->connection()->exec("UPDATE $table
                SET `$order_field` = `$order_field`-1
                WHERE `$order_field` > $from AND `$order_field` <= $to");
        } else {
            $result = (bool) $this->repository->connection()->exec("UPDATE $table
                SET `$order_field` = `$order_field`+1
                WHERE `$order_field` >= $to AND `$order_field` < $from");
        }
        
        if ($this->repository->update($entity) === false) {
            throw new EntityNotSavedException();
        }
        
        return $result;
        
    }
    
    protected function addOrder($entity) {
        $order_field = $entity->getOrderableField();
        $entity->$order_field = $this->nextAvailableOrder();
        $table = $this->repository->table();

        $result = $this->repository->connection()->fetchAssoc("SELECT $order_field FROM $table ORDER BY $order_field DESC LIMIT 1");
        
        //No results means  it's the first page so we pass 1
        $entity->$order_field = $result === false ? 1 : $result[$order_field] + 1;        
    }

}
