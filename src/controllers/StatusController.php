<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Controllers;

use Laasti\Response\ResponderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class StatusController
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $responder = null;
    protected $url = null;
    protected $container = null;
    protected $notification = null;

    
    public function __construct(ResponderInterface $responder, \Pixms\Url\UrlFactory $url, \Laasti\Notifications\NotificationService $notification, \League\Container\ContainerInterface $container)
    {
        $this->responder = $responder;
        $this->url = $url;
        $this->container = $container;
        $this->notification = $notification;
    }

    public function activate(Request $request)
    {
        $module = $request->attributes->get('_module');
        
        $id = $module->getIdentifier();
        $single = substr($id, 0, strlen($id)-1);
        
        $crudService = $module->getCrudService($request);
        $mapper = $module->getMapper($request);

        //TODO shouldn't access the container from the controller
        $this->container->add($id.'CrudService', $crudService)->withArgument($mapper);
        $service = $this->container->get($id.'CrudService');
        if ($service->activateEntityById($request->attributes->get('id'))) {
            $this->notification->success('The '.$single.' was successfully activated.');
        } else {
            $this->notification->error('The '.$single.' was already activated.');
        }

        return $this->responder->redirect('/'.$id);
    }

    public function deactivate(Request $request)
    {
        $module = $request->attributes->get('_module');

        $id = $module->getIdentifier();
        $single = substr($id, 0, strlen($id)-1);

        $crudService = $module->getCrudService($request);
        $mapper = $module->getMapper($request);

        //TODO shouldn't access the container from the controller
        $this->container->add($id.'CrudService', $crudService)->withArgument($mapper);
        $service = $this->container->get($id.'CrudService');
        if ($service->deactivateEntityById($request->attributes->get('id'))) {
            $this->notification->success('The '.$single.' was successfully deactivated.');
        } else {
            $this->notification->error('The '.$single.' was already deactivated.');
        }

        return $this->responder->redirect('/'.$id);
    }

}
