<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Controllers;

use Laasti\Response\ResponderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class ReorderController
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $responder = null;
    protected $url = null;
    protected $container = null;
    protected $notification = null;

    
    public function __construct(ResponderInterface $responder, \Pixms\Url\UrlFactory $url, \Laasti\Notifications\NotificationService $notification, \League\Container\ContainerInterface $container)
    {
        $this->responder = $responder;
        $this->url = $url;
        $this->container = $container;
        $this->notification = $notification;
    }

    public function handle(Request $request)
    {
        $module = $request->attributes->get('_module');
        
        $id = $module->getIdentifier();
        $post = $request->request;
        
        $crudService = $module->getCrudService($request);
        $mapper = $module->getMapper($request);

        //TODO shouldn't access the container from the controller
        $this->container->add($id.'CrudService', $crudService)->withArgument($mapper);
        $service = $this->container->get($id.'CrudService');
        $result = $service->reorderEntities($post->get('direction'), $post->get('from'), $post->get('to'));

        $this->responder->clearData();
        $this->responder->setData('result', $result);

        return $this->responder->json();
    }

}
