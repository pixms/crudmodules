<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Controllers;

use Laasti\Response\ResponderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class UpdateController
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $responder = null;
    protected $url = null;
    protected $container = null;
    protected $notification = null;

    
    public function __construct(ResponderInterface $responder, \Pixms\Url\UrlFactory $url, \Laasti\Notifications\NotificationService $notification, \League\Container\ContainerInterface $container)
    {
        $this->responder = $responder;
        $this->url = $url;
        $this->notification = $notification;
        $this->container = $container;
    }

    public function display(Request $request)
    {
        $module = $request->attributes->get('_module');
        
        $id = $module->getIdentifier();
        $single = substr($id, 0, strlen($id)-1);
        //TODO shouldn't access the container from the controller
        $formFactory = $this->container->get($module->getFormFactory($request));
        
        $crudService = $module->getCrudService($request);
        $mapper = $module->getMapper($request);

        $this->container->add($id.'CrudService', $crudService)->withArgument($mapper);
        $service = $this->container->get($id.'CrudService');
        $payload = $service->updateFromRequest($request);
        
        $this->responder->setData('page_title', 'Edit '.$single);

        $this->responder->setData('form', $formFactory->create('', $payload['data']));

        return $this->responder->view('form.twig');
    }

    public function submit($request) {

        $module = $request->attributes->get('_module');

        $id = $module->getIdentifier();
        $single = substr($id, 0, strlen($id)-1);

        $formFactory = $this->container->get($module->getFormFactory($request));
        $crudService = $module->getCrudService($request);
        $mapper = $module->getMapper($request);

        $this->container->add($id.'CrudService', $crudService)->withArgument($mapper);
        $service = $this->container->get($id.'CrudService');

        $payload = $service->updateFromRequest($request);

        if ($payload === true) {
            $this->notification->success('The '.$single.' was successfully edited.');
            return $this->responder->redirect('/'.$id);
        }
        
        $this->responder->setData('page_title', 'Edit '.$single);

        $this->responder->setData('form', $formFactory->create('', $payload['data'], $payload['errors']));


        return $this->responder->view('form.twig');
    }
    
}
