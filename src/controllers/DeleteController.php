<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Controllers;

use Laasti\Response\ResponderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class DeleteController
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $responder = null;
    protected $url = null;
    protected $container = null;
    protected $notification = null;

    
    public function __construct(ResponderInterface $responder, \Pixms\Url\UrlFactory $url, \Laasti\Notifications\NotificationService $notification, \League\Container\ContainerInterface $container)
    {
        $this->responder = $responder;
        $this->url = $url;
        $this->container = $container;
        $this->notification = $notification;
    }

    public function handle(Request $request)
    {
        $module = $request->attributes->get('_module');
        
        $id = $module->getIdentifier();
        $single = substr($id, 0, strlen($id)-1);
        
        $crudService = $module->getCrudService($request);
        $mapper = $module->getMapper($request);

        //TODO shouldn't access the container from the controller
        $this->container->add($id.'CrudService', $crudService)->withArgument($mapper);
        $service = $this->container->get($id.'CrudService');
        try {
            $service->deleteEntityById($request->attributes->get('id'));
            $this->notification->success('The '.$single.' was successfully deleted.');
        } catch (\Pixms\Spot\Exceptions\EntityNotFoundException $e) {
            $this->notification->error('The '.$single.' does not exist.');
        }

        return $this->responder->redirect('/'.$id);
    }

    /*
     *
        $successMessage = 'The '.$single.' was successfully activated.';
        $errorMessage = 'The '.$single.' was already activated.';

        //TODO what to do with all the interdependencies
        $container->add($id.'CrudService', $crudService)->withArgument($mapper);
        $container->add($id.'SuccessResponse', 'Pixms\Url\RedirectSuccessResponse')->withArguments(['Pixms\Url\UrlFactory','Laasti\Notifications\NotificationService', '/'.$id, $successMessage]);
        $container->add($id.'ErrorResponse', 'Pixms\Url\RedirectErrorResponse')->withArguments(['Pixms\Url\UrlFactory','Laasti\Notifications\NotificationService', '/'.$id, $errorMessage]);
        $container->add($id.'ActivateCommand', 'Pixms\Core\Command')->withArguments(array($id.'CrudService', 'activateEntityById', [$request->attributes->get('id')]));
        $container->add($id.'StatusActivateController', 'Pixms\\Url\\RedirectController')->withArguments([$id.'ActivateCommand', $id.'SuccessResponse', $id.'ErrorResponse']);

        $this->controller = $id.'StatusActivateController::respond';
     */
    

}
