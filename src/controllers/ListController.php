<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Controllers;

use Laasti\Response\ResponderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class ListController
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $responder = null;
    protected $url = null;
    protected $container = null;

    
    public function __construct(ResponderInterface $responder, \Pixms\Url\UrlFactory $url, \League\Container\ContainerInterface $container)
    {
        $this->responder = $responder;
        $this->url = $url;
        $this->container = $container;
    }

    public function display(Request $request)
    {
        $module = $request->attributes->get('_module');
        
        $id = $module->getIdentifier();
        $single = substr($id, 0, strlen($id)-1);
        
        $dataTable = $module->getDataTableFactory($request);
        $crudService = $module->getCrudService($request);
        $mapper = $module->getMapper($request);
        
        $this->responder->setData('page_title', $id);
        $this->responder->setData('links', [
            [
                'label' => 'Create a '.$single,
                'classes' => ['button', 'ok'],
                'url' => $this->url->create('/'.$id.'/create')
            ]
        ]);

        //TODO shouldn't access the container from the controller
        $this->container->add('Pixms\CrudModules\Decorators\PriorityDecorator')->withArguments(['Pixms\CrudModules\Decorators\StatusDecorator']);
        $this->container->add($id.'CrudService', $crudService)->withArguments([$mapper, 'Pixms\CrudModules\Decorators\PriorityDecorator']);
        $service = $this->container->get($id.'CrudService');
        $this->responder->setData('datatable', $service->buildDataTable($this->container->get($dataTable), $request));

        return $this->responder->view('datatables.twig');
    }

}
