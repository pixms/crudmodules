<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Interfaces;

use League\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @author Sonia
 */
interface ModuleInterface
{
    public function register(ContainerInterface $container);
    public function getIdentifier();
    public function getFormFactory(Request $request);
    public function getEntity(Request $request);
    public function getMapper(Request $request);
    public function getDataTableFactory(Request $request);
    public function getCrudService(Request $request);
    public function getNamespace();
}
