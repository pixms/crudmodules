<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules;

use League\Container\ContainerInterface;
use Pixms\CrudModules\Interfaces\ModuleInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of AbstractModule
 *
 * @author Sonia
 */
abstract class AbstractModule implements ModuleInterface
{
    protected $mapper = 'Pixms\Spot\PixmsMapper';
    protected $crudService = 'Pixms\CrudModules\CrudService';
    
    public function register(ContainerInterface $container) {}
    
    public function getFormFactory(Request $request) {
        return $this->formFactory;
    }
    
    public function getEntity(Request $request) {
        return $this->entity;
    }
    
    public function getMapper(Request $request) {
        return $this->mapper;
    }
    
    public function getDataTableFactory(Request $request) {
        return $this->dataTableFactory;
    }
    
    public function getCrudService(Request $request) {
        return $this->crudService;
    }
    
    public function getIdentifier() {
        return $this->identifier;
    }
    
    public function getNamespace() {
        $class = get_class($this);
        return substr($class, 0, strrpos($class, '\\'));
    }
}
