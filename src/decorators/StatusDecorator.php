<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Decorators;

/**
 * Description of StatusDecorator
 *
 * @author Sonia
 */
class StatusDecorator extends AbstractDecorator
{
    protected $twig;
    protected $url;
    
    public function __construct(\Pixms\Views\TwigRenderer $twig, \Pixms\Url\UrlFactory $url, $object = null)
    {
        $this->object = $object;
        $this->twig = $twig;
        $this->url = $url;
    }
    
    public function setTwig($twig) {
        $this->twig = $twig;
        return $this;
    }
    
    public function setUrlFactory($url) {
        $this->url = $url;
        return $this;
    }
    
    public function status() {
        return $this->twig->render('widgets/status.twig', array(
            'status' => $this->get('status'),
            'activate_url' => $this->url->create('/users/activate/'.$this->get('id')),
            'deactivate_url' => $this->url->create('/users/deactivate/'.$this->get('id'))
        ));
    }
}
