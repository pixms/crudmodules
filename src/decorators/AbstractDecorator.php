<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Decorators;

/**
 * Description of AbstractDecorator
 *
 * @author Sonia
 */
abstract class AbstractDecorator
{
    protected $object;
    
    public function __construct($object = null)
    {
        $this->object = $object;
    }
    
    public function setOriginalObject($newObject) {
        $originalObject = $this;
        $nextObject = $originalObject->getObject();
        while ($nextObject instanceof AbstractDecorator) {
            $originalObject = $nextObject;
            $nextObject = $nextObject->getObject();
        }
        $originalObject->object = $newObject;
        return $this;
    }
    
    public function getObject()
    {
        return $this->object;
    }
    
    public function getOriginalObject()
    {
        $object = $this->object;
        while ($object instanceof AbstractDecorator) {
            $object = $object->getOriginalObject();
        }
        return $object;
    }

    public function isCallable($method, $checkSelf = false)
    {
        //Check Decorators
        $object = $checkSelf ? $this : $this->object;
        while ($object instanceof AbstractDecorator) {
            if (is_callable(array($object, $method))) {
                return $object;
            }
            $object = $this->object;
        }
        
        //Check the original object
        if (is_callable(array($object, $method))) {
            return $object;
        }
        return false;
    }

    public function __call($method, $args)
    {
        if ($object = $this->isCallable($method)) {
            return call_user_func_array(array($object, $method), $args);
        }
        
        //Try magic getter for properties as last resort
        return $this->__get($method);
    }

    public function __get($property)
    {
        $object = $this->getOriginalObject();
        if (property_exists($object, $property)) {
            return $object->$property;
        } else if (method_exists($object, '__get')) {
            return $object->__get($property);
        }
        return null;
    }

    public function __set($property, $value)
    {
        $object = $this->getOriginalObject();
        $object->$property = $value;
        return $this;
    }

}
