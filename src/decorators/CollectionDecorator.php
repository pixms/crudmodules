<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Decorators;

/**
 * Description of CollectionDecorator
 *
 * @author Sonia
 */
class CollectionDecorator implements \Iterator, \Countable, \ArrayAccess
{
    protected $collection;
    protected $decorator;

    public function __construct($collection, AbstractDecorator $decorator)
    {
        if ($collection instanceof \IteratorAggregate) {
            $collection = $collection->getIterator();
        }
        if (!$collection instanceof \Iterator || !$collection instanceof \Countable || !$collection instanceof \ArrayAccess) {
            throw new \InvalidArgumentException('The collection passed to CollectionDecorator must implement Iterator or IteratorAggregate, Countable and ArrayAccess.');
        }
        
        $this->collection = $collection;
        $this->decorator = $decorator;
    }

    public function decorate($object) {
        //var_dump($object);
        $decorator = clone $this->decorator;
        return $decorator->setOriginalObject($object);
    }


    /**
     * Returns first result in set
     *
     * @return \Spot\Entity The first result in the set
     */
    public function first()
    {
        $this->rewind();

        return $this->current();
    }

    /**
     * Provides a string representation of the class
     * Brackets contain the number of elements contained
     * in the collection
     *
     * @return string
     *
     */
    public function __toString()
    {
        return __CLASS__ . "[" . $this->count() . "]";
    }

    /**
     * SPL - Countable
     *
     * @inheritdoc
     */
    public function count()
    {
        return $this->collection->count();
    }

    /**
     * SPL - Iterator
     *
     * @inheritdoc
     */
    public function current()
    {
        return $this->decorate($this->collection->current());
    }

    /**
     * SPL - Iterator
     *
     * @inheritdoc
     */
    public function key()
    {
        return $this->collection->key();
    }

    /**
     * SPL - Iterator
     *
     * @inheritdoc
     */
    public function next()
    {
        $this->collection->next();
    }

    /**
     * SPL - Iterator
     *
     * @inheritdoc
     */
    public function rewind()
    {
        $this->collection->rewind();
    }

    /**
     * SPL - Iterator
     *
     * @inheritdoc
     */
    public function valid()
    {
        return $this->collection->valid();
    }

    /**
     * SPL - ArrayAccess
     *
     * @inheritdoc
     */
    public function offsetExists($key)
    {
        return $this->collection->offsetExists($key);
    }

    /**
     * SPL - ArrayAccess
     *
     * @inheritdoc
     */
    public function offsetGet($key)
    {
        return $this->decorate($this->collection->offsetGet($key));
    }

    /**
     * SPL - ArrayAccess
     *
     * @inheritdoc
     */
    public function offsetSet($key, $value)
    {
        return $this->collection->offsetSet($key, $value);
    }

    /**
     * SPL - ArrayAccess
     *
     * @inheritdoc
     */
    public function offsetUnset($key)
    {
        $this->collection->offsetUnset($key);
    }

}
