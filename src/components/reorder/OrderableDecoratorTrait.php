<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Components\Reorder;

/**
 * Description of OrderableDecoratorTrait
 *
 * @author Sonia
 */
trait OrderableDecoratorTrait
{

    public function getOrderIcon()
    {
        return '<span class="icon-move-vertical" data-' . self::$orderableField . '="' . $this->get(self::$orderableField) . '"></span>';
    }

}
